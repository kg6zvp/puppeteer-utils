const hasSelector = async (page, selector) => page.$(selector) != null;

const saveAs = async (page, selector, name) => {
	page._client.send('Page.setDownloadBehavior', {
		behavior: 'allow',
		downloadPath: path.resolve(__dirname, name)
	});
	page.click(selector);
}

/*paginatedConfigInterface = {
	page, selectors: {
		nextPage,
		previousPage,
		item
	}
};*/

class PaginatedContent {
	constructor({ page, selectors, /*List<String>*/ itemProps, /*T function*/ itemConverter }) {
		this.page = page;
		this.selectors = selectors;
		this.itemProps = itemProps ? itemProps : [ 'id', 'href', 'class', 'src', 'title', 'alt' ];
		this.itemConverter = itemConverter ? itemConverter : (k) => k;
	}

	/*void*/ clickPreviousPage = async () => await this.page.click(this.selectors.previousPage);

	/*boolean*/ hasPreviousPage = async () => await hasSelector(this.page, this.selectors.previousPage);

	/*void*/ clickNextPage = async () => await this.page.click(this.selectors.nextPage);

	/*boolean*/ hasNextPage = async () => await hasSelector(this.page, this.selectors.nextPage);

	/*List<ItemClass>*/ getCurrentPage = () => {

	}
}

class InfiniteScrollContent {
	constructor({ page, selectors }) {
		this.page = page;
		this.selectors = selectors;
	}

	clickMore = async () => await this.page.click(this.selectors.more);

	hasMore = async () => await hasSelector(this.page, this.selectors.more);
}

module.exports = {
	hasSelector,
	PaginatedContent
}
